# frozen_string_literal: true

require 'courier_handlers/sendgrid_handler'

RSpec.describe CourierHandlers::SendgridHandler do
  let(:instance) { CourierHandlers::SendgridHandler.new }

  describe '.deliver' do
    subject { instance.deliver(fields) }

    before(:example) do
      CourierHandlers::SendgridHandler.api_key = 'api_key'
      CourierHandlers::SendgridHandler.default_from = 'default_from@example.com'

      # Makes sure no calls to SendGrid are actually made.
      allow(SendGrid::API).to receive(:new).and_return(sendgrid_api)
      allow(sendgrid_api).to receive(:client).and_return(sendgrid_client)
      allow(sendgrid_client).to receive(:mail).and_return(sendgrid_client)
      allow(sendgrid_client).to receive(:_).with('send').and_return(sendgrid_client)
      allow(sendgrid_client).to receive(:post).and_return(sendgrid_response)
    end

    let(:sendgrid_api) { double(:sendgrid_api) }
    let(:sendgrid_client) { double(:sendgrid_client) }

    let(:fields) do
      {
        'from' => 'from@example.com',
        'to' => 'to@example.com',
        'subject' => 'subject',
        'content' => 'content'
      }
    end

    let(:sendgrid_response) do
      SendGrid::Response.new(
        Struct
          .new(:code, :body, :to_hash)
          .new(202, '', 'x-message-id' => ['external_id'])
      )
    end

    it 'creates the SendGrid client with the correct credentials' do
      expect(SendGrid::API).to(
        receive(:new)
          .with(api_key: 'api_key')
          .and_return(sendgrid_api)
      )

      subject
    end

    it 'requests an email with the correct data' do
      expect(sendgrid_client).to(
        receive(:post)
          .with(request_body: {
                  'content' => [{ 'type' => 'text/plain', 'value' => 'content' }],
                  'from' => { 'email' => 'from@example.com' },
                  'personalizations' => [{ 'to' => [{ 'email' => 'to@example.com' }] }],
                  'subject' => 'subject'
                })
          .once
      )

      subject
    end

    it 'returns a normalized delivery response' do
      expect(subject).to be_a(CourierHandlers::DeliveryResponse)
      expect(subject.success).to eq(true)
      expect(subject.external_id).to eq('external_id')
    end

    context 'when the `from` field is not specified' do
      before(:example) { fields.delete('from') }

      it 'uses the configured `default_from` value' do
        expect(instance).to receive(:_deliver) do |fields|
          expect(fields['from']).to eq('default_from@example.com')
        end

        subject
      end
    end
  end

  describe '.callback' do
    subject { CourierHandlers::SendgridHandler.callback(args) }

    let(:args) do
      {
        'sg_message_id' => 'external_id',
        'event' => event
      }
    end

    let(:event) { 'delivered' }

    it 'returns a normalized callback request' do
      expect(subject).to be_a(CourierHandlers::CallbackRequest)
      expect(subject.success).to eq(true)
      expect(subject.external_id).to eq('external_id')
      expect(subject.error).to be_nil
      expect(subject.ignore?).to eq(false)
    end

    context 'when the message status is `dropped`' do
      let(:event) { 'dropped' }

      it 'sets the `success` attribute to `false`' do
        expect(subject.success).to eq(false)
      end
    end

    context 'when the message status is `bounce`' do
      let(:event) { 'bounce' }

      it 'sets the `success` attribute to `false`' do
        expect(subject.success).to eq(false)
      end
    end

    context 'when the message status is something else' do
      let(:event) { 'other' }

      it 'sets the callback request to ignore' do
        expect(subject).to be_a(CourierHandlers::CallbackIgnoreRequest)
        expect(subject.ignore?).to eq(true)
      end
    end

    context 'when an error message is included in the request' do
      before(:example) { args['reason'] = 'error message' }

      it 'sets the `error` attribute' do
        expect(subject.error).to eq('error message')
      end
    end
  end
end
