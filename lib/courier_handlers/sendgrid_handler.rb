# frozen_string_literal: true

require 'courier_handlers/email_handler'
require 'sendgrid-ruby'

module CourierHandlers
  # Handler for the SendGrid third-party service.
  class SendgridHandler < EmailHandler
    class << self
      attr_accessor :api_key,
                    :default_from

      def configure
        yield self
      end
    end

    # ref. https://sendgrid.com/docs/API_Reference/Event_Webhook/event.html
    #
    # An example of SendGrid's callback request following a delivery:
    #     {
    #        "email": "example@test.com",
    #        "timestamp": 1513299569,
    #        "smtp-id": "<14c5d75ce93.dfd.64b469@ismtpd-555>",
    #        "event": "delivered",
    #        "category": "cat facts",
    #        "sg_event_id": "sg_event_id",
    #        "sg_message_id": "sg_message_id",
    #        "response": "250 OK"
    #     }
    #
    # @params args [Hash] SendGrid's callback body.
    # @return (see super)
    def self.callback(args)
      success = case args['event']
                when 'delivered' then true
                when 'dropped', 'bounce' then false
                else return CourierHandlers::CallbackIgnoreRequest.new
                end

      CourierHandlers::CallbackRequest.new(
        success,
        args['sg_message_id'],
        args['reason']
      )
    end

    protected

    # The `from` field, if unset, will default to the configured one.
    #
    # Aside from the fields defined in the parent class, this handler does not normalize any other
    # fields.
    def normalize_delivery_fields(fields)
      fields['from'] ||= self.class.default_from
      super
    end

    # ref. https://sendgrid.com/docs/API_Reference/api_v3.html
    #
    # An example of SendGrid's delivery response to a new message:
    #     HTTP/1.1 202 Accepted
    #     Server: nginx
    #     Date: Sat, 09 Feb 2019 16:29:43 GMT
    #     Content-Type: text/plain; charset=utf-8
    #     Content-Length: 0
    #     Connection: keep-alive
    #     X-Message-Id: WO4GKq3_TCJEUDRIc4AsyA
    #     Access-Control-Allow-Origin: https://sendgrid.api-docs.io
    #     Access-Control-Allow-Methods: POST
    #     Access-Control-Allow-Headers: Authorization, Content-Type, On-behalf-of, x-sg-elas-acl
    #     Access-Control-Max-Age: 600
    #     X-No-CORS-Reason: https://sendgrid.com/docs/Classroom/Basics/API/cors.html
    #
    # Note: Headers are stored in arrays inside `NET::HTTPResponse`s.
    #
    # Email fields are all supported, except for `cc` and `bcc` (currently ignored). The `from`
    # field is optional - if not specified, the configured default email address will be used.
    def _deliver(fields)
      sendgrid_response = request_delivery(fields)

      CourierHandlers::DeliveryResponse.new(
        true,
        sendgrid_response.headers['x-message-id'].first
      )
    end

    private

    def request_delivery(fields)
      SendGrid::API
        .new(api_key: self.class.api_key)
        .client
        .mail
        ._('send')
        .post(request_body: mail(fields).to_json)
    end

    def mail(fields)
      SendGrid::Mail.new(
        SendGrid::Email.new(email: fields['from']),
        fields['subject'],
        SendGrid::Email.new(email: fields['to']),
        SendGrid::Content.new(type: 'text/plain',
                              value: fields['content'])
      )
    end
  end
end
