# frozen_string_literal: true

module CourierHandlers
  module SendgridHandlers
    VERSION = '0.11.0'
  end
end
